# Fridge Organiser Whiteboard

Wipe-clean whiteboards for detailing what is in each drawer or shelf of a freezer or fridge.
Stick on your refrigerated storage units with a dry wipe marker and stay organised as lab members change or stocks/samples rotate.

The svg and dxf supplied have a blank slot for adding the title text and the fridge identifier if you prefer not to write this with the marker. We assumed the fridge sign would be a number or a letter, if it is not you will need to change the size of the box.


###  Materials
 -  A4 flexible magnetic whiteboards (we sourced MagFlex® Lite A4 Flexible Magnetic Sheet - Gloss White / Dry-Wipe from UK supplier https://www.magnetexpert.com/ for 2.45 GBP per sheet)
 - Self-adhesive vinyl in a colour of your choice

### Equipment
 - Vinyl cutter, we used the Silhouette Cameo 4

### Design Files
 - Three drawer design ([svg](./Fridge-3Drawer.svg), [dxf](./Fridge-3Drawer.dxf))
 - Four drawer design ([svg](./Fridge-4Drawer.svg), [dxf](./Fridge-4Drawer.dxf))

### Instructions

1. Load the dxf or svg into your vinyl cutter software after adding any text you wish to the design
2. Once cut, remove the waste vinyl and weed the letters
 <img   src="./images/cut-before-weeding.jpg" width="400" alt="cut vinyl before weeding">
 <img   src="./images/cut-after-weeding.jpg" width="400" alt="cut vinyl after weeding">

3. Once the vinyl is cut we recomend using a transfer sheet to move it onto the whiteboard without misaligning it. It is still tricky to get lined up and without bubbles so take your time and assume it won't work first time. 

 <img   src="./images/transfer-sheet.jpg" width="400" alt="Vinyl on transfer sheet">


4. The magnetic sheet can be cut very easily with standard scissors. You could also use your vinyl cutter or a strong paper cutter.
5. Pop on a fridge or freezer door along with a whiteboard marker

 <img   src="./images/finished-sign.jpg" width="400" alt="Finished sign on freezer door">
  <img   src="./images/demo.jpg" width="400" alt="Filled in sign on freezer door">


