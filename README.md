# Vinyl Cut Lab Signs

Vinyl cutter designs for around the laboratory

## Signs

| Description                               | Image                                                                  |
|:-----------------------------------------:|:----------------------------------------------------------------------:|
| [Fridge Organiser](/Freezer%20Organiser/) | <img src="./Freezer%20Organiser/images/finished-sign.jpg" width="200"> |

## License

All designs are licensed under a [Creative Commons CC0](https://creativecommons.org/share-your-work/public-domain/cc0/) Public Domain Waiver
